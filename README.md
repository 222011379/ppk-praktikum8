# PPK-Praktikum 8 : Android Studio

## Identitas

```
Nama : Maulana Pandudinata
NIM  : 222011379
Kelas: 3SI3

```

## Deskripsi

Android Studio adalah Integrated Development Environment (IDE) resmi untuk pengembangan aplikasi Android, yang didasarkan pada IntelliJ IDEA. 
Android Studio menawarkan banyak fitur yang meningkatkan produktivitas Anda dalam membuat aplikasi Android, seperti: 

Sistem build berbasis Gradle yang fleksibel. 
Emulator yang cepat dan kaya fitur. 
Lingkungan terpadu tempat Anda bisa mengembangkan aplikasi untuk semua perangkat Android.
Terapkan Perubahan untuk melakukan push pada perubahan kode dan resource ke aplikasi yang sedang berjalan tanpa memulai ulang aplikasi.
Template kode dan integrasi GitHub untuk membantu Anda membuat fitur aplikasi umum dan mengimpor kode sampel. 
Framework dan alat pengujian yang lengkap. 
Alat lint untuk merekam performa, kegunaan, kompatibilitas versi, dan masalah lainnya. 
Dukungan C++ dan NDK. 
Dukungan bawaan untuk Google Cloud Platform, yang memudahkan integrasi Google Cloud Messaging dan App Engine. 


## Kegiatan Praktikum

## 1. Hasil install dan run empty activities
![](dokumentasi/1.png)
